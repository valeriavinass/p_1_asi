import HTTP from "@/common/http";

const resource = "users";

export default {
  async findAll() {
    const response = await HTTP.get(resource);
    return response.data;
  },
  async findAllPrivate() {
    const response = await HTTP.get(`${resource}/private`);
    return response.data;
  },
  async findOne(id) {
    const user = (await HTTP.get(`${resource}/${id}`)).data;
    return user;
  },
  async changeStatus(id, active) {
    var response = null;

    if (active == true) {
      response = await HTTP.put(`${resource}/${id}/active`);
    } else {
      response = await HTTP.delete(`${resource}/${id}/active`);
    }
    return response.data;
  },
  async deleteUser(id) {
    return await HTTP.delete(`${resource}/${id}`);
  },
};
